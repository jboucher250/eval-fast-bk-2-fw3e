namespace FortCodeExercises.Exercise1.Models
{
    public class Machine
    {
        private MachineType MachineType { get; }

        public string Name { get; }

        public string Description { get; set; }

        public Color Color { get; set; }

        public Color TrimColor { get; set; }

        public Machine(MachineType machineType)
        {
            MachineType = machineType;
            Name = MachineType.ToString();
            Color = GetColor(machineType);
            TrimColor = GetTrimColor(machineType);

            Description = $" {Color} {Name} [{GetMaxSpeed(MachineType)}].";
        }

        private static Color GetColor(MachineType machineType)
        {
            switch (machineType)
            {
                case MachineType.Tractor:
                    return Color.Green;
                case MachineType.Bulldozer:
                    return Color.Red;
                case MachineType.Crane:
                    return Color.Blue;
                case MachineType.Car:
                    return Color.Brown;
                case MachineType.Truck:
                    return Color.Yellow;
                default:
                    return Color.White;
            }
        }

        private static Color GetTrimColor(MachineType machineType)
        {
            switch (machineType)
            {
                case MachineType.Tractor:
                    return Color.Gold;
                case MachineType.Crane:
                    return Color.White;
                default:
                    return Color.None;
            }
        }

        private int GetMaxSpeed(MachineType machineType)
        {
            switch (machineType)
            {
                case MachineType.Tractor:
                    return 90;
                case MachineType.Bulldozer:
                    return 80;
                case MachineType.Crane:
                    return 75;
                default:
                    return 70;
            }
        }
    }
}
﻿namespace FortCodeExercises.Exercise1.Models
{
    public enum Color
    {
        None,
        White,
        Blue,
        Red,
        Brown,
        Yellow,
        Green,
        Gold
    }
}
﻿namespace FortCodeExercises.Exercise1.Models
{
    public enum MachineType
    {
        Tractor,
        Bulldozer,
        Crane,
        Car,
        Truck
    }
}
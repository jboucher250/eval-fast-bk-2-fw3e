# Notes

## Changes
It is a good habit to organize models in a directory named Models in order to isolate these data model objects from other classes such as those that perform business logic.
 
Color and MachineType are better suited as enums because what we know of them at this point is that they appear to be a finite and static list of values.
 
Some colors are omitted because they are not used in any end-result. They can be added at a later time if needed rather than kept in the hopes of being needed in the future.

A constructor is used to force the instantiation of the Machine type to only one specific way and that is with a specified MachineType.

Logic has been removed from the properties as this typically becomes hard to maintain and couples the property definition to the logic associated with deriving them.

Unnecessary derivation logic has been removed due to the simplicity of the requirements and the readability of using enums.

Note: Do not reverse the name/use of a bool as was originally in the example of hasMaxSpeed being passed into noMax.

In multiple places, impossible Boolean operations were originally used and have been removed, such as impossible type and color combinations.

Do not use reserved words such as Type to name a property/field.

Use proper casing as shown now for methods, properties, etc